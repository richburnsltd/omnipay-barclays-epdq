<?php

namespace Omnipay\BarclaysEpdq;

use Symfony\Component\HttpFoundation\ParameterBag;
use Omnipay\Common\Helper;

/**
 * BarclaysEpdq Delivery & Invoicing Data
 */
class Recipient
{
    /**
     * @var \Symfony\Component\HttpFoundation\ParameterBag
     */
    protected $parameters;

    /**
     * Create a new BankAccount object using the specified parameters
     *
     * @param array $parameters An array of parameters to set on the new object
     */
    public function __construct($parameters = null)
    {
        $this->initialize($parameters);
    }

    /**
     * Initialize the object with parameters.
     *
     * If any unknown parameters passed, they will be ignored.
     *
     * @param array $parameters An associative array of parameters
     *
     * @return $this
     */
    public function initialize($parameters = null)
    {
        $this->parameters = new ParameterBag();
        Helper::initialize($this, $parameters);
        return $this;
    }

    public function getParameters()
    {
        return $this->parameters->all();
    }

    protected function getParameter($key)
    {
        return $this->parameters->get($key);
    }

    protected function setParameter($key, $value)
    {
        $this->parameters->set($key, $value);
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->getParameter('AccountNumber');
    }

    /**
     * @param string $value the maximum accepted length is 10
     */
    public function setAccountNumber($value)
    {
        $this->setParameter('AccountNumber', $value);
    }

    /**
     * @return string
     */
    public function getDateOfBirth()
    {
        return $this->getParameter('DateOfBirth');
    }

    /**
     * @param string $value Format DD/MM/YYYY
     *
     * @return \Omnipay\BarclaysEpdq\Recipient
     */
    public function setDateOfBirth($value)
    {
        return $this->setParameter('DateOfBirth', $value);
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->getParameter('LastName');
    }

    /**
     * @param $value
     *
     * @return \Omnipay\BarclaysEpdq\Recipient
     */
    public function setLastName($value)
    {
        return $this->setParameter('LastName', $value);
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->getParameter('Postcode');
    }

    /**
     * @param $value
     *
     * @return \Omnipay\BarclaysEpdq\Recipient
     */
    public function setPostcode($value)
    {
        return $this->setParameter('Postcode', $value);
    }
}
